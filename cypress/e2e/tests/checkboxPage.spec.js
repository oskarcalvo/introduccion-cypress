import CheckBoxPage from "../pages/CheckboxPage";
const checkboxPage = new CheckBoxPage();

context('Check and uncheck elements', () => {
  it('Check and uncheck elements', () => {
    checkboxPage.visit();
    checkboxPage.validate();
    checkboxPage.uncheck();
    checkboxPage.check();
  })
})