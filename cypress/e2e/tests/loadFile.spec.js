import LoadFile from "../pages/LoadFile";
const loadFile = new LoadFile();

context('Load a picture', () => {
  it('Load a picture', () => {
    loadFile.visit();
    loadFile.validate();
    loadFile.upload();
    loadFile.isUploaded();
  })
});

