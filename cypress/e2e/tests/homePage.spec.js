import HomePage from "../pages/HomePage";
const homePage = new HomePage();

context('Enter the home page and check that it works', () => {
  it('Visit the home page', () => {
    homePage.visit();
    homePage.validateStatusCode(200);
    homePage.validate('Welcome to the Internet');
  });
});
