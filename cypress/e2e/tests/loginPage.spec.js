import LoginPage from "../pages/LoginPage";
import LogedUserPage from "../pages/LogedUserPage";
const loginPage = new LoginPage();
const logedUserPage = new LogedUserPage();

context('Login', () => {
  it('Login successfully', () => {
    loginPage.visit();
    loginPage.typeUsername("tomsmith");
    loginPage.typePassword("SuperSecretPassword!");
    loginPage.clickLogin();
    logedUserPage.isLogin();
  })

})