import RedirectPage200 from "../pages/RedirectPage200";
const redirectPage2000 = new RedirectPage200();

context('Visit status code 200', () => {
  it('Visit page and get a 200 response status code', () => {
    redirectPage2000.validateCode(200);
  })
})