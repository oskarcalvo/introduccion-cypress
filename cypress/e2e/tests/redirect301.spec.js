import RedirectPage301 from "../pages/RedirectPage301";
const redirectPage301 = new RedirectPage301();

context('Visit and get a status code 301', () => {
  it('Visit page and get a 301 response status code', () => {
    redirectPage301.validateCode(301);
  })
});