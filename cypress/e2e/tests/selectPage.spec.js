import SelectPage from "../pages/SelectPage";
const selectPage = new SelectPage();

context('Select one element', () => {
  it('Try to select one element', () => {
    selectPage.visit();
    selectPage.validate();
    selectPage.selectElement();
  })
})