export default class RedirectPage200 {
  path = "/status_codes/200"

  validateCode(status) {
    cy.request({
      url: this.path,
    }).then((resp) => {
      expect(resp.status).to.eq(status)
    })
  }
}