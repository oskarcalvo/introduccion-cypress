export default class LoadFile {
  path = "/upload";

  visit() {
    cy.visit(this.path);
  }

  validate() {
    cy.get('h3').should('have.text', 'File Uploader');
  }


  upload() {
    cy.get("input[type=file]")
      .attachFile("elefante.jpg");

    cy.get("input[id=file-submit]").click();
  }

  isUploaded() {
    cy.get("h3").should("have.text", "File Uploaded!");
  }

}