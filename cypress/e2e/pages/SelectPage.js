export default class SelectPage {
  path = "/dropdown";

  visit() {
    cy.visit(this.path);
  }

  validate() {
    cy.get("h3").should('have.text', 'Dropdown List');
  }

  selectElement() {
    cy.get("select").select("2");
  }
}