export default class RedirectPage301 {
  path = "/status_codes/301";

  validateCode(status) {
    cy.request({
      url: this.path,
    }).then((resp) => {
      expect(resp.status).to.eq(status)
    })
  }
}