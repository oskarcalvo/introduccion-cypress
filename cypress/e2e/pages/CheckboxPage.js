export default class CheckBoxPage {
  path = "/checkboxes";

  visit() {
    cy.visit(this.path)
  }

  validate() {
    cy.get("h3").should('have.text', 'Checkboxes');
  }

  uncheck() {
    cy.get('[type="checkbox"]').eq(1).uncheck();
  }

  check() {
    cy.get('[type="checkbox"]').eq(0).check();
    // first() funciona igual que eq(0)
    // cy.get('[type="checkbox"]').first().check();
  }
}