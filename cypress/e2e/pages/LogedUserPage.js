export default class LogedUserPage {
  path = "/secure"

  isLogin() {
    cy.get('h4.subheader').should('have.text', 'Welcome to the Secure Area. When you are done click logout below.');
  }
}