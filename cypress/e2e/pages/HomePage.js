export default class HomePage {
  path = "/"

  visit() {
    cy.visit(this.path);

  }

  validateStatusCode(status_code) {
    cy.request({
      url: this.path,
    }).then((resp) => {
      expect(resp.status).to.eq(status_code)
    })
  }

  validate(title) {
    cy.get('.heading').should('have.text', title);
  }
}